.. Data Cleaner documentation master file, created by
   sphinx-quickstart on Sat Sep 14 09:08:06 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pydatacleaner
========================================

A utility designed to process/parse/clean scalars, especially text. (note: in active development)

.. toctree::
   :maxdepth: 3
   :caption: Contents:

Overview
========

This is the API documentation for the pydatacleaner Python package. The source for the package along with a more user-friendly README with general use examples are located at the `GitLab project page <https://gitlab.com/krcrouse/datacleaner>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
