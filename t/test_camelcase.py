import pytest
import os, sys, shutil
from fileinput import close
import traceback
import datetime
import random
import string
import time

sys.path.insert(0,'.')
import datacleaner

def setup_module():
    pass

class TestCamel():

    def setup_class(self):

        self.cleaner = datacleaner.CamelCase()

        self.to_clean = [
		    ("test_case","testCase"),
		    (" test_case","testCase"),
		    ("test_case ","testCase"),
		    (" test_case ","testCase"),
		    ("test","test"),
		    ("many_many_words","manyManyWords"),
		    ("any_kind_of_string","anyKindOfString"),
		    ("numbers_2_and_55_with_000","numbers2And55With000"),
		    ("10_four","10Four"),
            ("ten_four","tenFour"),
            ("ten_4","ten4"),
		    ("json_data","jsonData"),
		    ("user_id","userId"),
		    ("aa_abbb","aaAbbb"),
            # test spaces and such to snake case
            ("numbers 2 and 55 with 000", "numbers2And55With000"),
            ("test Case", "testCase"),
            ("Test Case", "TestCase"),
            (" Test Case", "TestCase"),
            ("Test Case ", "TestCase"),
            (" test Case ", "testCase"),
            ("test     Case A", "testCaseA"),
            ("test", "test"),
            ("Test", "Test"),
            ("Many Many Words", "ManyManyWords"),
            ("many Many    Words", "manyManyWords"),
            ("Any Kind  Of_string", "AnyKindOfString"),
            ("JSON     Data", "JSONData"),
            ("user  ID", "userID"),
            ("middle ATM Junk", "middleATMJunk"),
            ("AA   Abbb", "AAAbbb"),
            ("10 Four","10Four"),
            ("ten    four","tenFour"),
            ("ten 4","ten4"),
            ('Plural ACROs', "PluralACROs"),
            ('Plural   ACRONYMS', "PluralACRONYMS"),
            ('Non Plural ACR Ost', "NonPluralACROst"),
            ('user IDs', 'userIDs'),
            ('ATM Number','ATMNumber'),
            ('my JSON Data','myJSONData'),
            ('This     FRIEND  Asymmetry','ThisFRIENDAsymmetry'),
        ]

        self.to_tokenize = [
		    ("numbers2and55with000", ["numbers","2","and","55","with","000"]),
		    ("testCase", ["test","Case"]),
		    ("TestCase", ["Test","Case"]),
		    (" TestCase", ["Test","Case"]),
		    ("TestCase ", ["Test","Case"]),
		    (" testCase ", ["test","Case"]),
            ("testCaseA", ["test","Case","A"]),
		    ("test", ["test"]),
		    ("Test", ["Test"]),
		    ("ManyManyWords", ["Many","Many","Words"]),
		    ("manyManyWords", ["many","Many","Words"]),
		    ("AnyKindOf_string", ["Any","Kind","Of_string"]),
		    ("JSONData", ["JSON","Data"]),
		    ("userID", ["user","ID"]),
            ("middleATMJunk", ["middle","ATM","Junk"]),
		    ("AAAbbb", ["AA","Abbb"]),
		    ("10Four",["10","Four"]),
            ("tenFour",["ten","Four"]),
            ("ten4",["ten","4"]),
            ('PluralACROs', ["Plural","ACROs"]),
            ('PluralACRONYMS', ["Plural","ACRONYMS"]),
            ('NonPluralACROst', ["Non","Plural","ACR","Ost"]),
            ('ATMs', ['ATMs']),
            ('userIDs', ['user','IDs']),
            ('ATMNumber',["ATM","Number"]),
            ('myJSONData',["my","JSON","Data"]),
            ('ThisFRIENDAsymmetry',["This","FRIEND","Asymmetry"]),
        ]

        self.alt_tokenize = [
		    ("numbers2and55with000", ["numbers","2","and","55","with","000"]),
		    ("testCase", ["test","Case"]),
		    ("TestCase", ["Test","Case"]),
		    (" TestCase", ["Test","Case"]),
		    ("TestCase ", ["Test","Case"]),
		    (" testCase ", ["test","Case"]),
            ("testCaseA", ["test","Case","A"]),
		    ("test", ["test"]),
		    ("Test", ["Test"]),
		    ("ManyManyWords", ["Many","Many","Words"]),
		    ("manyManyWords", ["many","Many","Words"]),
		    ("AnyKindOf_string", ["Any","Kind","Of_string"]),
		    ("JSONdata", ["JSON","data"]),
		    ("userID", ["user","ID"]),
            ("middleATMjunk", ["middle","ATM","junk"]),
		    ("AAAbbb", ["AAA","bbb"]),
		    ("10Four",["10","Four"]),
            ("tenFour",["ten","Four"]),
            ("ten4",["ten","4"]),
            ('PluralACROs', ["Plural","ACROs"]),
            ('PluralACRONYMS', ["Plural","ACRONYMS"]),
            ('NonPluralACROst', ["Non","Plural","ACRO","st"]),
            ('ATMs', ['ATMs']),
            ('userIDs', ['user','IDs']),
            ('ATMnumber',["ATM","number"]),
            ('myJSONdata',["my","JSON","data"]),
            ('ThisFRIENDasymmetry',["This","FRIEND","asymmetry"]),
        ]

    def test_clean(self):
        for raw, cleaned in self.to_clean:
            print(raw,'->',self.cleaner.clean(raw))
            assert self.cleaner.clean(raw) == cleaned, self.__class__.__name__ + " incorrectly translated " + str(raw) + " to " + str(self.cleaner.clean(raw)) +", expected " + str(cleaned)

    def test_tokenize(self):
        for raw, tokens in self.to_tokenize:
            tokenized = self.cleaner.tokenize(raw)
            print(raw,'->',tokenized)
            assert len(tokenized) == len(tokens), self.__class__.__name__ + " tokenizer incorrectly tokenized " + str(raw) + " to "+str(len(tokenized))+" tokens, expected " +str(len(tokens)) + "\n\tFull tokenization result: " + str(tokenized) + "\n\tExpected: " + str(tokens)
            for i in range(len(tokens)):
                assert tokenized[i] == tokens[i], self.__class__.__name__ + " tokenizer incorrectly tokenized " + str(raw) + " to "+str(tokenized)+", expected " +str(tokens)


    def test_alt_tokenize(self):
        altcleaner = self.cleaner.copy()
        altcleaner.alternate_acronym = True

        for raw, tokens in self.alt_tokenize:
            tokenized = altcleaner.tokenize(raw)
            print(raw,'->',tokenized)
            assert len(tokenized) == len(tokens), self.__class__.__name__ + " alternate tokenizer incorrectly tokenized " + str(raw) + " to "+str(len(tokenized))+" tokens, expected " +str(len(tokens)) + "\n\tFull tokenization result: " + str(tokenized) + "\n\tExpected: " + str(tokens)
            for i in range(len(tokens)):
                assert tokenized[i] == tokens[i], self.__class__.__name__ + " alternate tokenizer incorrectly tokenized " + str(raw) + " to "+str(tokenized)+", expected " +str(tokens)

        # now make sure that copy and setting the alternate_acronym on the object worked as expected

        assert not datacleaner.CamelCase.alternate_acronym, "Setting the alternate acronymn flag on the alternate cleanear appears to have modified the class variable"

        assert not self.cleaner.alternate_acronym, "Setting the alternate acronymn flag on the alternate cleanear appears to have modified the source object variable"

        # this would fail if copy didn't work right and the altcleaner modification changed self
        self.test_tokenize()
