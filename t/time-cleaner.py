import string
import random
import sys
import time
import re

sys.path.append('.')
import datacleaner

def main():
    test_largeset()


floatre = re.compile(r'-?(\d*?\.)?\d+$')
spacere = re.compile(r'\s*$')

def basicstrip(val):
    val = val.strip()
    if floatre.match(val):
        return(float(val))
    if spacere.match(val):
        return(None)
    return(val)


def test_largeset():

     cleaner = datacleaner.DataCleaner(
         null_values = ['NA', 'na', 'N/A', 'n/a', 'NULL', 'null', 'None', 'none', 'nan', 'NaN', '#N/A'],
     )

     chars = string.ascii_lowercase + string.digits + string.whitespace
     weights = [1] * 26 + [2] * 10 + [3] * len(string.whitespace)

     start = time.time()
     bigset = []
     for i in range(10000000):
         bigset.append("".join(random.choices(chars, weights=weights, k=random.randint(3,12))))
     end = time.time()
     print("Sample strings:\n\t", bigset[:10])
     print("\n\nGenerate strings: ", (end-start))

     start = time.time()
     for s in bigset:
         basicstrip(s)
     end = time.time()
     print("Basic Strip: ", (end-start))

     start = time.time()
     skipped = 0
     processed = 0
     for s in bigset:
         try:
             cleaner.clean(s)
             processed += 1
         except OverflowError as oe:
             # this happens when the randomly generated value is infinity
             skipped += 1
             pass
         except Exception as e:
             print("WHAT ", s)
             raise
     end = time.time()
     print("10M cleans: ", (end-start))
     print("\tProcessed:", processed, "\n\tSkipped:", skipped)

main()
