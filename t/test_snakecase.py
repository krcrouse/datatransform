import pytest
import os, sys, shutil
from fileinput import close
import traceback
import datetime
import random
import string
import time

sys.path.append('.')
import datacleaner

def setup_module():
    pass

class TestSnake():

    def setup_class(self):
        self.to_snake_cases = [
            # test various camel cases to snake case
            ("numbers2and55with000", "numbers_2_and_55_with_000"),
            ("testCase", "test_case"),
            ("TestCase", "test_case"),
            (" TestCase", "test_case"),
            ("TestCase ", "test_case"),
            (" testCase ", "test_case"),
            ("testCaseA", "test_case_a"),
            ("test", "test"),
            ("Test", "test"),
            ("ManyManyWords", "many_many_words"),
            ("manyManyWords", "many_many_words"),
            ("AnyKindOf_string", "any_kind_of_string"),
            ("JSONData", "json_data"),
            ("userID", "user_id"),
            ("middleATMJunk", "middle_atm_junk"),
            ("AAAbbb", "aa_abbb"),
            ("10Four","10_four"),
            ("tenFour","ten_four"),
            ("ten4","ten_4"),
            ('PluralACROs', "plural_acros"),
            ('PluralACRONYMS', "plural_acronyms"),
            ('NonPluralACROst', "non_plural_acr_ost"),
            ('ATMs', 'atms'),
            ('userIDs', 'user_ids'),
            ('ATMNumber','atm_number'),
            ('myJSONData','my_json_data'),
            ('ThisFRIENDAsymmetry','this_friend_asymmetry'),
            # test spaces and such to snake case
            ("numbers 2 and 55 with 000", "numbers_2_and_55_with_000"),
            ("test Case", "test_case"),
            ("Test Case", "test_case"),
            (" Test Case", "test_case"),
            ("Test Case ", "test_case"),
            (" test Case ", "test_case"),
            ("test     Case A", "test_case_a"),
            ("test", "test"),
            ("Test", "test"),
            ("Many Many Words", "many_many_words"),
            ("many Many    Words", "many_many_words"),
            ("Any Kind  Of_string", "any_kind_of_string"),
            ("JSON     Data", "json_data"),
            ("user  ID", "user_id"),
            ("middle ATM Junk", "middle_atm_junk"),
            ("AA   Abbb", "aa_abbb"),
            ("10 Four","10_four"),
            ("ten    Four","ten_four"),
            ("ten 4","ten_4"),
            ('Plural ACROs', "plural_acros"),
            ('Plural   ACRONYMS', "plural_acronyms"),
            ('Non Plural ACR Ost', "non_plural_acr_ost"),
            ('user IDs', 'user_ids'),
            ('ATM Number','atm_number'),
            ('my JSON Data','my_json_data'),
            ('This     FRIEND  Asymmetry','this_friend_asymmetry'),
        ]

    def teardown_class(self):
        pass


    def test_cases(self):

        cleaner = self.default = datacleaner.SnakeCase()

        for raw, snake in self.to_snake_cases:
            assert cleaner.clean(raw) == snake, "Snake case translates " + raw + " to " + snake
