import pytest
import os, sys, shutil
from fileinput import close
import traceback
import datetime
import random
import string
import time

sys.path.append('.')
import datacleaner

def setup_module():
    pass

class TestDataCleaner():

    def setup_class(self):
        pass

    def teardown_class(self):
        pass


    def test_time(self):

        cleaner = datacleaner.DataCleaner( data_type=datetime.time )
        times = {
            '15:00': datetime.time(15,0),
            '2:00 pm': datetime.time(14,0),
            '2:00': datetime.time(2,0),
            '2:00PM': datetime.time(14,0),
            '0:00': datetime.time(0,0),
            '12:00': datetime.time(12,0),
            '12:00pm': datetime.time(12,0),
            '12:00 a.m.': datetime.time(0,0),
            '18:55:30.35': datetime.time(18,55,30, 350000),
            '18:55:30': datetime.time(18,55,30),
            '6:55:30 pm': datetime.time(18,55,30),
            }

        for raw, test in times.items():
            print("Test",raw)
            assert cleaner.clean(raw) == test

        cleaner.data_type = datetime.date

        dates = {
            # these should all parse because they include the month in text
			'03-Aug-06': datetime.date(2006,8,3),
			'03-Aug-2006': datetime.date(2006,8,3),
			'3-Aug-06': datetime.date(2006,8,3),
			'3-Aug-2006': datetime.date(2006,8,3),
			'3-August-06': datetime.date(2006,8,3),
			'3-August-2006': datetime.date(2006,8,3),
            'Aug-03-06': datetime.date(2006,8,3),
            'Aug-03-2006': datetime.date(2006,8,3),
            'Monday, 3 of August 2006': datetime.date(2006,8,3),
            '03Aug06': datetime.date(2006,8,3),
            '03Aug2006': datetime.date(2006,8,3),
#            '3Aug06': datetime.date(2006,8,3),
#            '3Aug2006': datetime.date(2006,8,3),
#			'060803': datetime.date(2006,8,3),
            #'06/08/03': datetime.date(2006,8,3),
            # iso format - variants on yyy - month - day
            '2006-08-03': datetime.date(2006,8,3),
			'20060803': datetime.date(2006,8,3),
            20060803: datetime.date(2006,8,3),
			'2006/08/03': datetime.date(2006,8,3),
            # month - day - year
			'08/03/06': datetime.date(2006,8,3),
			'08/03/2006': datetime.date(2006,8,3),
            '8/3/06': datetime.date(2006,8,3),
            '8/3/2006': datetime.date(2006,8,3),
            '12.18.97': datetime.date(1997,12,18),
            '12.25.2006': datetime.date(2006,12,25),
            '7-5-2000': datetime.date(2000,7,5),
            # excluded because they cannot be distinguished from iso format
#            '080306': datetime.date(2006,8,3),
#            '08032006': datetime.date(2006,8,3),

            #excluded because they cannot be distinguished from mm/day/year format
#            '03/08/06': datetime.date(2006,8,3),
#            '03/08/2006': datetime.date(2006,8,3),
#            '3/8/06': datetime.date(2006,8,3),
#            '3/8/2006': datetime.date(2006,8,3),
#            '030806': datetime.date(2006,8,3),
#            '03082006': datetime.date(2006,8,3),

        }
        for raw, test in dates.items():
            assert cleaner.clean(raw) == test


        cleaner.data_type = datetime.datetime
        datetimes = {
            '1994-11-05T08:15:30-05:00': datetime.datetime(1994, 11, 5, 8, 15, 30,
                                                           tzinfo= datetime.timezone(datetime.timedelta(hours=-5))),
            '1994-11-05T13:15:30Z':datetime.datetime(1994, 11, 5, 13, 15, 30,
                                                           tzinfo= datetime.timezone(datetime.timedelta(hours=0))),
            '1994-11-05T08:03:30-05:00': datetime.datetime(1994, 11, 5, 8, 3, 30,
                                                           tzinfo= datetime.timezone(datetime.timedelta(hours=-5))),
            '1994-11-05T13:03:30Z':datetime.datetime(1994, 11, 5, 13, 3, 30,
                                                     tzinfo= datetime.timezone(datetime.timedelta(hours=0))),
            '2006-08-03 18:55:30': datetime.datetime(2006,8,3,18,55,30),
            '03-Aug-2006 6:55:30 pm': datetime.datetime(2006,8,3,18,55,30),
        }
        for raw, test in datetimes.items():
            assert cleaner.clean(raw) == test



    def test_nulls(self):
        default_null_values = ['NA', 'na', 'N/A', 'n/a', 'NULL', 'null', 'None', 'none', 'nan', 'NaN', '#N/A']

        cleaner = datacleaner.DataCleaner(
            null_values = default_null_values,
        )

        for ok in ('a','123', 123, 15.0, 2.66, -3.55, datetime.date(2017,3,5)):
            val = cleaner.clean(ok)
            assert ok is not None

        for nullable in default_null_values:
            assert nullable is not None
            assert cleaner.clean(nullable) is None

        additional = ['Toothpaste', -999, 0, 15.2, 'Woof', 'foo bar']
        for ok in additional:
            val = cleaner.clean(ok)
            assert ok is not None

        cleaner.add_null_values(additional)
        for nullable in additional:
            assert nullable is not None
            assert cleaner.clean(nullable) is None

        assert cleaner.clean(None) is None



    def test_translations(self):

        initial_translations = {-999:None, 888:'Missing', '-999': 'N/A', '888': 5, None: 0, '777': 'INVALID string num not converting to num first', 777: 'num convert is accurate', 'xxx': 0}
        cleaner = datacleaner.DataCleaner(
            translations = initial_translations
        )
        # this verifies the expected cases that types are converted before translations,
        # so -999 and '-999' should relate to the same output (though we also dont trust order for hashes)
        assert cleaner.clean('777') == 'num convert is accurate'
        assert cleaner.clean(-999) == None
        assert cleaner.clean('-999') == None
        assert cleaner.clean(888) == 'Missing'
        assert cleaner.clean('888') == 'Missing'
        assert cleaner.clean(None) == 0
        assert cleaner.clean('xxx') == 0

        #
        # test that adding translations after initialization works
        assert cleaner.clean(999) == 999
        cleaner.add_translations({999:None})
        assert cleaner.clean(999) == None

    def test_transliteration(self):

        basic_transliterations = [
            ('a','X'),
            [' ','_']
        ]
        cleaner = self.default = datacleaner.DataCleaner(
            transliterations= basic_transliterations,
        )

        assert cleaner.clean('aaa') == 'XXX'
        assert cleaner.clean('woof and bark') == 'woof_Xnd_bXrk'

        #cleaner.add_transliterations(r'(\w)(\W)')
